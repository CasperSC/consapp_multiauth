﻿using System;
using System.Collections.Generic;
using System.IO;
using ConsoleApplication1.Interfaces;

namespace ConsoleApplication1.Data
{
    public class AuthDataRepository : IAuthDataRepository
    {
        private readonly string _filePath;

        public AuthDataRepository(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentException("filePath");
            }
            _filePath = filePath;
        }

        public void Save(IEnumerable<AuthData> authData)
        {
            using (StreamWriter stream = new StreamWriter(_filePath))
            {
                foreach (AuthData data in authData)
                {
                    stream.WriteLine(data.Login + Consts.AuthDataFileFormat.LoginPasswordSeparator + data.Password);
                }
            }
        }

        public AuthData[] Load()
        {
            List<AuthData> result = new List<AuthData>(64);

            using (StreamReader stream = new StreamReader(_filePath))
            {
                while (!stream.EndOfStream)
                {
                    string line = stream.ReadLine();
                    if (line != null)
                    {
                        string[] res = line.Split(Consts.AuthDataFileFormat.LoginPasswordSeparator);
                        if (res.Length > 1)
                        {
                            result.Add(new AuthData(res[0], res[1]));
                        }
                        else
                        {
                            throw new Exception(string.Format("Неверный формат строки - \"{0}\"", line));
                        }
                    }
                }
            }

            return result.ToArray();
        }
    }
}