﻿namespace ConsoleApplication1.Data
{
    public class AuthData
    {
        public AuthData()
            : this(string.Empty, string.Empty)
        {
        }

        public AuthData(string login, string password)
        {
            Login = login;
            Password = password;
        }

        public string Login { get; set; }

        public string Password { get; set; }
    }
}