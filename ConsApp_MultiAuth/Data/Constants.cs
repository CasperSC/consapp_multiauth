﻿namespace ConsoleApplication1.Data
{
    public class Consts
    {
        /// <summary>
        /// Константы, относящиеся к формату файлов, хранящих данные авторизации.
        /// </summary>
        public class AuthDataFileFormat
        {
            /// <summary>
            /// Разделитель логина и пароля в файле.
            /// </summary>
            public const char LoginPasswordSeparator = ';';
        }
    }
}