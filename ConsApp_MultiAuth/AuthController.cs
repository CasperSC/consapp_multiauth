﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using ConsoleApplication1.Communication;
using ConsoleApplication1.Data;
using ConsoleApplication1.Interfaces;

namespace ConsoleApplication1
{
    internal class AuthController
    {
        //Для каждого потока будет свой экземпляр объекта, реализующий IAuthorizationHelper
        private readonly ThreadLocal<IAuthorizationHelper> _authHelpers;
        private readonly IAuthDataRepository _repository;

        public AuthController(IAuthDataRepository repository)
        {
            if (repository == null)
            {
                throw new ArgumentNullException("repository");
            }

            _repository = repository;
            _authHelpers = new ThreadLocal<IAuthorizationHelper>(() => new AuthorizationHelper());
        }

        public Task<string[]> Run()
        {
            return Task<string[]>.Factory.StartNew(() =>
            {
                var data = _repository.Load();
                ConcurrentBag<string> lines = new ConcurrentBag<string>();

                Parallel.ForEach(data, (authData) =>
                {
                    var athorization = _authHelpers.Value;

                    var text = string.Format(
                        "Поток: {1}, Номер AuthorizationHelper: {2}{0}Логин: {3}, Пароль: {4}{0}------------------",
                        Environment.NewLine, Thread.CurrentThread.ManagedThreadId,
                        athorization.Id, authData.Login, authData.Password);
                    lines.Add(text);

                    athorization.Authorize(authData.Login, authData.Password);
                });

              return lines.ToArray();
            });
        }
    }
}