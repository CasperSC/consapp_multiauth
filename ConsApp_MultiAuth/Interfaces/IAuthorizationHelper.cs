﻿namespace ConsoleApplication1.Interfaces
{
    public interface IAuthorizationHelper
    {
        bool Authorize(string login, string password);

        int Id { get; }
    }
}