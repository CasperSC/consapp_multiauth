﻿using System.Collections.Generic;
using ConsoleApplication1.Data;

namespace ConsoleApplication1.Interfaces
{
    public interface IAuthDataRepository
    {
        void Save(IEnumerable<AuthData> authData);

        AuthData[] Load();
    }
}