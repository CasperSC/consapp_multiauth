﻿using System;

namespace ConsoleApplication1.Utils
{
    public static class RandomHelper
    {
        private static readonly object _sync = new object();

        private static readonly Random _random;

        static RandomHelper()
        {
            _random = new Random();
        }

        public static bool RandomBool()
        {
            lock (_sync)
            {
                return _random.Next(0, 2) == 1;
            }
        }

        public static int Next(int minValue, int maxValue)
        {
            lock (_sync)
            {
                return _random.Next(minValue, maxValue);
            }
        }
    }
}