﻿using System.Threading;
using ConsoleApplication1.Interfaces;
using ConsoleApplication1.Utils;

namespace ConsoleApplication1.Communication
{
    internal class AuthorizationHelper : IAuthorizationHelper
    {
        private static int _counter;
        private readonly int _id;

        public AuthorizationHelper()
        {
            _id = Interlocked.Increment(ref _counter);
        }

        //Вот тут нужно писать код авторизации, либо создать другую реализацию интерфейса
        //и в Main подменить эту реализацию на свою.
        public bool Authorize(string login, string password)
        {
            Thread.Sleep(1000);
            return RandomHelper.RandomBool();
        }

        public int Id
        {
            get { return _id; }
        }
    }
}