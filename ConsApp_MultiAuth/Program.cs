﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using ConsoleApplication1.Data;
using ConsoleApplication1.Interfaces;

namespace ConsoleApplication1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string repositoryFileName = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "AuthData.txt");

            var repository = new AuthDataRepository(repositoryFileName);

            CreateAuthDataFileIfNotExists(repository, repositoryFileName);

            var controller = new AuthController(repository);

            Task<string[]> task = controller.Run(); //Запускаем имитацию авторизаций
            TaskAwaiter<string[]> awaiter = task.GetAwaiter();
            Console.Write("Пожалуйста подождите");
            while (!awaiter.IsCompleted)
            {
                //Лень заморачиваться, сойдёт и так.
                Thread.Sleep(250);
                Console.Write(".");
            }
            Console.Clear();
            
            Array.Sort(task.Result); //Сортируем результат выполнения кода в методе Run
            Console.WriteLine("Вывод отсортированных значений для удобства " +
                              "анализа номеров объектов и их соответствия номерам потоков");

            Array.ForEach(task.Result, s => Console.WriteLine(s));

            Console.WriteLine("Нажмите любую клавишу, чтобы завершить работу приложения.");
            Console.ReadKey();
        }

        //Это так, чтобы не заморачиваться. В реале врядли стоит так делать. В реале данные уже есть, либо специально
        //создаются и сохраняются перед использованием, а это так, для быстроты и удобства проверки работы
        private static void CreateAuthDataFileIfNotExists(IAuthDataRepository repository, string repositoryFileName)
        {
            if (!File.Exists(repositoryFileName))
            {
                const int Quantity = 16;
                var data = new AuthData[Quantity];

                for (var i = 0; i < Quantity; i++)
                {
                    var number = i + 1;
                    data[i] = new AuthData("login " + number, "password" + number);
                }

                repository.Save(data);
            }
        }
    }
}